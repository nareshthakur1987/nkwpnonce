#WordPress Nonces

Complete object oriented package of WP NONCE. All possible methods.

##Where to add the code

add to your active theme's functions.php:


// Autoload files using Composer autoload
require __DIR__ . '/vendor/autoload.php';

##How to use

1. Create a nonce

$NkWPNonce = new \Nkwpnonce\NkWpNonce();
$nonceCreate = $NkWPNonce->nkwpn_wp_create_nonce();
For example:

<a href='pluginurl.php?action=some_action&_wpnonce=<?php echo $create_none; ?>'>Custom Action</a>

2. Verify a nonce

$NkWPNonce = new \Nkwpnonce\NkWpNonce();
$nonceVerify = $NkWPNonce->nkwpn_wp_verify_nonce();

3. Display 'Are you sure you want to do this?' message to confirm the action being taken.

$NkWPNonce = new \Nkwpnonce\NkWpNonce();
$nonceAys = $NkWPNonce->nkwpn_wp_nonce_ays();

4. Verify a nonce passed in an AJAX request

$NkWPNonce = new \Nkwpnonce\NkWpNonce();
$checkAjaxRefer = $NkWPNonce->nkwpn_check_ajax_referer();

5. Retrieve or display the referer hidden form field

$WPNonce = new \Nkwpnonce\NkWpNonce();
$wpReferField = $WPNonce->nkwpn_wp_referer_field();

6. Add a nonce to a URL

$NkWPNonce = new \Nkwpnonce\NkWpNonce();
$nonceUrl = $NkWPNonce->nkwpn_wp_nonce_url();

7. Test either if the current request carries a valid nonce, or if the current request was referred from an administration screen

$NkWPNonce = new \Nkwpnonce\NkWpNonce();
$checkAdminRefer = $NkWPNonce->nkwpn_check_admin_referer();

8. Add a nonce to a form

$NkWPNonce = new \Nkwpnonce\NkWpNonce();
$nonceField = $NkWPNonce->nkwpn_wp_nonce_field();