<?php
/**
 * @package Implementing all methods of WP Nonce in OOPs
 */
namespace Nkwpnonce;
class NkWpNonce {
    
    /**
     * @param string $action required. The nonce action. Default: None
     */
    public function nkwpn_wp_nonce_ays( $action ) {
        return wp_nonce_ays( $action );
    }

    /**
     * Data based Nonce Time, User id, action
     * @param string/int $action optional. Action name. default: -1
     * @return string The one use form token.
     */
    public function nkwpn_wp_create_nonce( $action=-1 ) {
        return wp_create_nonce( $action );
    }

    /**
     * Verify nonce
     * @param string     $nonce  default none required
     * @param string/int $action default -1
     * @return bool/int Boolean false if the nonce is invalid. else 1 – if the nonce has been generated in the past 12 hours or less OR 2 – if the nonce was generated between 12 and 24 hours ago.
     */
    public function nkwpn_wp_verify_nonce( $nonce, $action=-1 ) {
        return wp_verify_nonce( $nonce, $action );
    }
    
    /**
     * Retrieves or displays the nonce hidden form field.
     * @param string  $action  deafult -1. Action name.
     * @param string  $name    deafult '_wpnonce'.
     * @param bool $referer deafult true.
     * @param bool $echo    deafult true.
     * @return string hidden form field inserted into the html.
     */
    public function nkwpn_wp_nonce_field( $action=-1, $name='_wpnonce', $referer=true, $echo=true ) {
        return wp_nonce_field( $action, $name, $referer, $echo );
    }
    /**
     * Fetch url with nonce added to query arguments.
     * @param string $actionurl default none required
     * @param string $action    default -1.
     * @param string $name      default _wpnonce.
     * @return string nonce enabled url.
     */
    public function nkwpn_wp_nonce_url( $actionurl, $action=-1, $name='_wpnonce' ) {
        return wp_nonce_url( $actionurl, $action, $name );
    }
    
    
    /**
     * Check either nonce is valid or if it's refered by admin screen
     * @param string $action    default -1
     * @param string $query_arg optional default '_wpnonce'
     * @return To return boolean true, in the case of the obsolete usage, the current request must be referred from
     * an administration screen; in the case of the prefered usage, the nonce must be sent and valid. Otherwise the
     * function dies with an appropriate message ("Are you sure you want to do this?" by default).
     */
    public function nkwpn_check_admin_referer( $action=-1, $query_arg='_wpnonce' ) {
        return check_admin_referer( $action, $query_arg );
    }

    /**
     * Retrieves or displays the referer hidden form field.
     * @param bool $echo default true
     * @return string
     */
    public function nkwpn_wp_referer_field( $echo=true ) {
        return wp_referer_field( $echo );
    }
    
    /**
     * @param string  $action default -1
     * @param string  $query_arg default false
     * @param bool $die default true
     * @return bool If parameter $die is set to false, this function will return a boolean of true if the check
     * passes or false if the check fails.
     */
    public function nkwpn_check_ajax_referer( $action=-1, $query_arg=false, $die=true ) {
        return check_ajax_referer( $action, $query_arg, $die );
    }
    
    
    
}