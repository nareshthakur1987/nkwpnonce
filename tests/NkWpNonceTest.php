<?php

use Nkwpnonce\NkWpNonce;

class NkWpNonceTest extends \PHPUnit_Framework_TestCase
{
    public function test_nkwpn_wp_nonce_ays($action='log-out') {
        $NkWpNonce = new NkWpNonce();
        $nonceAys = $NkWpNonce->test_nkwpn_wp_nonce_ays($action='log-out');
        $this->assertEquals(1, $nonceAys);
    }

    public function test_nkwpn_wp_referer_field() {
        $NkWpNonce = new NkWpNonce();
        $wpReferField = $NkWpNonce->test_nkwpn_wp_referer_field();
        $this->assertEquals(1, $wpReferField);
    }

    public function test_nkwpn_wp_verify_nonce($nonce='my-nonce') {
        $NkWpNonce = new NkWpNonce();
        $nonceVerify = $NkWpNonce->test_nkwpn_wp_verify_nonce($nonce='my-nonce');
        $this->assertEquals(1, $nonceVerify);
    }

    public function test_nkwpn_wp_nonce_url($actionurl='http://site.ru/url') {
        $NkWpNonce = new NkWpNonce();
        $nonceUrl = $NkWpNonce->test_nkwpn_wp_nonce_url($actionurl='http://site.ru/url');
        $this->assertEquals(1, $nonceUrl);
    }

    public function test_nkwpn_wp_nonce_field() {
        $NkWpNonce = new NkWpNonce();
        $nonceField = $NkWpNonce->test_nkwpn_wp_nonce_field();
        $this->assertEquals(1, $nonceField);
    }

    public function test_nkwpn_check_admin_referer() {
        $NkWpNonce = new NkWpNonce();
        $checkAdminRefer = $NkWpNonce->test_nkwpn_check_admin_referer();
        $this->assertEquals(1, $checkAdminRefer);
    }

    public function test_nkwpn_wp_create_nonce() {
        $NkWpNonce = new NkWpNonce();
        $nonceCreate = $NkWpNonce->test_nkwpn_wp_create_nonce();
        $this->assertEquals(1, $nonceCreate);
    }

    public function test_nkwpn_check_ajax_referer() {
        $NkWpNonce = new NkWpNonce();
        $checkAjaxRefer = $NkWpNonce->test_nkwpn_check_ajax_referer();
        $this->assertEquals(1, $checkAjaxRefer);
    }

    

}

function wp_nonce_ays($action='log-out') {
    return 1;
}
function wp_nonce_url($actionurl='http://site.ru/url') {
    return 1;
}
function wp_nonce_field() {
    return 1;
}
function check_admin_referer() {
    return 1;
}
function wp_referer_field() {
    return 1;
}
function wp_verify_nonce($nonce='my-nonce') {
    return 1;
}
function wp_create_nonce() {
    return 1;
}
function check_ajax_referer() {
    return 1;
}